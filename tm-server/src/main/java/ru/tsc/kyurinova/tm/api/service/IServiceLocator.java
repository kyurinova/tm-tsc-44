package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.api.service.dto.*;
import ru.tsc.kyurinova.tm.api.service.model.*;

public interface IServiceLocator {

    @NotNull
    ITaskDTOService getTaskService();

    @NotNull
    IProjectDTOService getProjectService();

    @NotNull
    IProjectTaskDTOService getProjectTaskService();

    @NotNull
    IUserDTOService getUserService();

    @NotNull
    ISessionDTOService getSessionService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IConnectionService getConnectionService();

    @NotNull
    IAdminDataService getAdminDataService();

}
