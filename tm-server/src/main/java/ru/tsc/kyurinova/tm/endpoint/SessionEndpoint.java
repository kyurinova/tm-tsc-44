package ru.tsc.kyurinova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.api.endpoint.ISessionEndpoint;
import ru.tsc.kyurinova.tm.api.service.IServiceLocator;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull SessionDTO openSession(
            @NotNull
            @WebParam(name = "login", partName = "login")
                    String login,
            @NotNull
            @WebParam(name = "password", partName = "password")
                    String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void closeSession(
            @NotNull
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().close(session);
    }

}
