package ru.tsc.kyurinova.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminUserEndpoint {

    @WebMethod
    void removeUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @WebParam(name = "entity", partName = "entity")
                    UserDTO entity
    );

    @WebMethod
    void clearUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    void removeByIdUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    void removeByIndexUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void removeByLoginUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    );

    @WebMethod
    @NotNull UserDTO createUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password
    );

    @WebMethod
    @NotNull UserDTO createAdminUser(
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password
    );

    @WebMethod
    @NotNull UserDTO createUserEmail(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password,
            @Nullable
            @WebParam(name = "email", partName = "email")
                    String email
    );

    @WebMethod
    @NotNull UserDTO createUserRole(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password,
            @Nullable
            @WebParam(name = "role", partName = "role")
                    Role role
    );

    @WebMethod
    @Nullable UserDTO setPasswordUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "userId", partName = "userId")
                    String userId,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password
    );

    @WebMethod
    @Nullable UserDTO updateUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "userId", partName = "userId")
                    String userId,
            @Nullable
            @WebParam(name = "firstName", partName = "firstName")
                    String firstName,
            @Nullable
            @WebParam(name = "lastName", partName = "lastName")
                    String lastName,
            @Nullable
            @WebParam(name = "middleName", partName = "middleName")
                    String middleName
    );

    @WebMethod
    @Nullable UserDTO lockUserByLogin(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    );

    @WebMethod
    @Nullable UserDTO unlockUserByLogin(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    );
}
