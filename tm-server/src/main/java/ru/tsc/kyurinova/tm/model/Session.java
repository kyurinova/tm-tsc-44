package ru.tsc.kyurinova.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "tm_session")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Session extends AbstractOwnerModel implements Cloneable {

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Nullable
    @Column(name = "time_stamp")
    private Long timestamp;

    @Nullable
    @Column
    private String signature;

}